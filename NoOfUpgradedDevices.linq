<Query Kind="Expression">
  <Connection>
    <ID>82d88dfb-68cd-46e5-a9ce-65c7b988103a</ID>
    <Persist>true</Persist>
    <Server>jygi9ig3yy.database.windows.net</Server>
    <SqlSecurity>true</SqlSecurity>
    <Database>PIEPS</Database>
    <NoPluralization>true</NoPluralization>
    <UserName>sqladministrator</UserName>
    <Password>AQAAANCMnd8BFdERjHoAwE/Cl+sBAAAARLwRB/koWku71ZN/tcuMegAAAAACAAAAAAADZgAAwAAAABAAAADkh94NK8dm92peVbs93zGrAAAAAASAAACgAAAAEAAAAMFZJ2UTCv2Ji9kfS6IOCZoQAAAA1CzbLyBrXkFqHXzBAgO91hQAAAAGakKAf6HDOEUhDWXJP5QM9ZzCqg==</Password>
    <DbVersion>Azure</DbVersion>
  </Connection>
</Query>

(from u in UPGRADE
where 
	d.PurchaseDate > new DateTime(2013, 9, 1) &&
	d.PurchaseDate < new DateTime(2014, 9, 1)
group d by new {
	d.PurchaseDate.GetValueOrDefault().Year,
	d.PurchaseDate.GetValueOrDefault().Month
	
} into dg
select new {
	Month = dg.Key.Year + "-" + ((dg.Key.Month<10)? "0" : "") + dg.Key.Month,
	All = dg.Count(),
	DSP = dg.Count(x => x.DeviceTypeID == 1),
	DSPTour = dg.Count(x => x.DeviceTypeID == 2),
	DSPSport = dg.Count(x => x.DeviceTypeID == 6),
	DSPPro = dg.Count(x => x.DeviceTypeID == 7)
}).OrderBy(stat=> stat.Month)