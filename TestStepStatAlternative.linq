<Query Kind="Statements">
  <Connection>
    <ID>73d9d0cb-aa68-4515-8753-8f10f540f0cf</ID>
    <Persist>true</Persist>
    <Server>u0pvq7nrpz.database.windows.net</Server>
    <SqlSecurity>true</SqlSecurity>
    <Database>PiepsTest</Database>
    <UserName>sqladministrator</UserName>
    <Password>AQAAANCMnd8BFdERjHoAwE/Cl+sBAAAAB0sQ1pUA0UiKz0uwGovIaQAAAAACAAAAAAADZgAAwAAAABAAAADI3SC9w2Pq0nzEAtBtv67uAAAAAASAAACgAAAAEAAAAMUD0Y29QwtB9g9iBqzci/IQAAAA1/b4EFTFkyuvA9FTi9wOwRQAAAA4VfcQ6Agv7kJ/ORUYQ3mT4YaSVQ==</Password>
    <DbVersion>Azure</DbVersion>
  </Connection>
</Query>

var start = new DateTime(2013,11, 1);
var stop = new DateTime(2013,11,10);
var results = 
    from 
		ts in TESTSTEPRESULTs 	
    where
		ts.TestResult.TestRunResult.DateTime > start && ts.TestResult.TestRunResult.DateTime < stop &&
		ts.TestResult.TestRunResult.FacilityID == 3	
	select new {
		Id = ts.ID,
		TestStepId = ts.TestStepID,
		Name = ts.TESTSTEP.Name,
		Result = ts.Result,
		Status = ts.Status
	};
	
	
	
var results2 = 
	from ts in results	
    group ts by ts.TestStepId
         into tsg
         select new 
         {
             Id = tsg.Key,
             //Name = tsg.FirstOrDefault().TESTSTEP.Name,
             //Results = tsg.Select(x =>
             //                     new 
             //                     {
             //                         Id = x.ID,
             //                         TestStepId = x.TestStepID,
             //                         Result = x.Result,
             //                         StatusId = x.Status
             //                     }),
             Count = tsg.Count(),
             Ok = tsg.Count(x=> x.Status == 1),
             Failed = tsg.Count(x => x.Status == 2),
             Error = tsg.Count(x => x.Status == 3),                                 
             Avg = tsg.Average(x => (float)x.Result)
         };
		 
results2.Dump();


