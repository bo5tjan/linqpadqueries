<Query Kind="Expression">
  <Connection>
    <ID>73d9d0cb-aa68-4515-8753-8f10f540f0cf</ID>
    <Persist>true</Persist>
    <Server>u0pvq7nrpz.database.windows.net</Server>
    <SqlSecurity>true</SqlSecurity>
    <Database>PiepsTest</Database>
    <UserName>sqladministrator</UserName>
    <Password>AQAAANCMnd8BFdERjHoAwE/Cl+sBAAAAB0sQ1pUA0UiKz0uwGovIaQAAAAACAAAAAAADZgAAwAAAABAAAADI3SC9w2Pq0nzEAtBtv67uAAAAAASAAACgAAAAEAAAAMUD0Y29QwtB9g9iBqzci/IQAAAA1/b4EFTFkyuvA9FTi9wOwRQAAAA4VfcQ6Agv7kJ/ORUYQ3mT4YaSVQ==</Password>
    <DbVersion>Azure</DbVersion>
  </Connection>
</Query>

from t in TESTRESULTs
join trr in TESTRUNRESULTs on t.TestRunResultID equals trr.ID
where
 trr.Status != 1 &&
 trr.DateTime.Year == 2013 && trr.DateTime.Month == 11 && trr.DateTime.Day == 11 &&
 trr.FacilityID == 1                                                              
group t by t.TestID
into grp
select new 
{
    TestId = grp.Key,
    TestName = grp.FirstOrDefault().TEST.Name,                                  
    Devices = grp.Select(x => x.TestRunResult.MCUID).Distinct().Count(),
    Result = grp.Count(),
    Error = grp.Count(x=> x.Status == 3),
    Faild = grp.Count(x => x.Status == 2)                                                                    
}