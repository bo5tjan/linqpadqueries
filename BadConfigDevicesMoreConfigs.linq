<Query Kind="Statements">
  <Connection>
    <ID>93be6b62-7a6f-4f23-a554-1841168d3858</ID>
    <Persist>true</Persist>
    <Server>fkmhr5y20c.database.windows.net</Server>
    <SqlSecurity>true</SqlSecurity>
    <Database>PiepsProduction</Database>
    <NoPluralization>true</NoPluralization>
    <UserName>sqladministrator</UserName>
    <Password>AQAAANCMnd8BFdERjHoAwE/Cl+sBAAAAB0sQ1pUA0UiKz0uwGovIaQAAAAACAAAAAAADZgAAwAAAABAAAAAMBxS8fgq+ukz//Un//1/tAAAAAASAAACgAAAAEAAAAOmwEFn1/OqRhJQsOWrp/XUYAAAAQhHmvq5TDdk4utzbrdny//OC/mIug+V7FAAAABzFEl1nxeeYyxT/3+AqwRu48k6h</Password>
    <DbVersion>Azure</DbVersion>
  </Connection>
</Query>

//not ok devices more configs
var notOk =
from sc in DSP2CONFIG
where 
sc.AntTuneSrReceiveXT0 == 0 || sc.AntTuneSrReceiveXT1 == 0 ||
sc.AntTuneSrReceiveYT0 == 0 || sc.AntTuneSrReceiveYT1 == 0 ||
sc.AntTuneSrReceiveZT0 == 0 || sc.AntTuneSrReceiveZT1 == 0 ||
sc.AntTuneVoltReceiveXT0 == 0 || sc.AntTuneVoltReceiveXT1 == 0 ||
sc.AntTuneVoltReceiveYT0 == 0 || sc.AntTuneVoltReceiveYT1 == 0 ||
sc.AntTuneVoltReceiveZT0 == 0 || sc.AntTuneVoltReceiveZT1 == 0 ||
(sc.DSPPhT1 == 0 && sc.DSPPhT0 == 0) || 
sc.DSPFdev== 0 ||
(sc.DSPFactorX == 1 && sc.DSPFactorY == 1 && sc.DSPFactorZ == 1 )
select sc.MCUID;

//get number of configs for not ok devices
var noOfConfigs = 
from sc in DSP2CONFIG
where notOk.Contains(sc.MCUID)
group sc by sc.MCUID into grp 
select new {
	MCUID = grp.Key,
	Count = grp.Count()
	
};
// more configs
var moreConfig = 
from d in noOfConfigs
where d.Count > 1
select d;
//oneConfig.Dump();

//last Config not ok (expensive operation
var lastConfig = 
from d in moreConfig
join sc in DSP2CONFIG on d.MCUID equals sc.MCUID
group sc by sc.MCUID into grp
select new {
	MCUID = grp.Key,
	Config = grp.OrderByDescending(s => s.DateTime).First()
};

var lastProductionGrp = 
from d in moreConfig
join p in PRODUCTION on d.MCUID equals p.MCUID
group p by p.MCUID into grp
select new {
	MCUID = grp.Key,
	Production = grp.OrderByDescending(s => s.DateTime).First()
};

var lastProd = 
from p in lastProductionGrp
select p.Production;

//lastProd.Dump();

var reallynotOk =
from s in lastConfig
where 
s.Config.AntTuneSrReceiveXT0 == 0 || s.Config.AntTuneSrReceiveXT1 == 0 ||
s.Config.AntTuneSrReceiveYT0 == 0 || s.Config.AntTuneSrReceiveYT1 == 0 ||
s.Config.AntTuneSrReceiveZT0 == 0 || s.Config.AntTuneSrReceiveZT1 == 0 ||
s.Config.AntTuneVoltReceiveXT0 == 0 || s.Config.AntTuneVoltReceiveXT1 == 0 ||
s.Config.AntTuneVoltReceiveYT0 == 0 || s.Config.AntTuneVoltReceiveYT1 == 0 ||
s.Config.AntTuneVoltReceiveZT0 == 0 || s.Config.AntTuneVoltReceiveZT1 == 0 ||
(s.Config.DSPPhT1 == 0 && s.Config.DSPPhT0 == 0) || 
s.Config.DSPFdev== 0 ||
(s.Config.DSPFactorX == 1 && s.Config.DSPFactorY == 1 && s.Config.DSPFactorZ == 1 )
select s.Config;

//reallynotOk.Dump();


var badSC = reallynotOk.ToList();
var badP = lastProd.ToList();
var q=
from sc in badSC
join p in badP on sc.MCUID equals p.MCUID
join d in DEVICE on sc.MCUID equals d.MCUID
join f in FACILITY on p.FacilityID equals f.FacilityID
select new {
Facility= f.Name,
ProductionDate = p.DateTime,
DeviceType =d.DEVICETYPE.Name,
d.MCUID,
d.SerialNumber,
PCBSerialNumber = d.ProductionSerialNumber,
sc.AntTuneSrReceiveXT0, sc.AntTuneSrReceiveXT1,
sc.AntTuneSrReceiveYT0, sc.AntTuneSrReceiveYT1,
sc.AntTuneSrReceiveZT0, sc.AntTuneSrReceiveZT1,
sc.AntTuneVoltReceiveXT0, sc.AntTuneVoltReceiveXT1,
sc.AntTuneVoltReceiveYT0, sc.AntTuneVoltReceiveYT1,
sc.AntTuneVoltReceiveZT0, sc.AntTuneVoltReceiveZT1,
sc.DSPPhT1, sc.DSPPhT0,
sc.DSPFdev,
sc.DSPFactorX, sc.DSPFactorY, sc.DSPFactorZ
};
q.Dump();
