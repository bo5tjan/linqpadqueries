<Query Kind="Statements">
  <Connection>
    <ID>93be6b62-7a6f-4f23-a554-1841168d3858</ID>
    <Persist>true</Persist>
    <Server>fkmhr5y20c.database.windows.net</Server>
    <SqlSecurity>true</SqlSecurity>
    <Database>PiepsProduction</Database>
    <NoPluralization>true</NoPluralization>
    <UserName>sqladministrator</UserName>
    <Password>AQAAANCMnd8BFdERjHoAwE/Cl+sBAAAAB0sQ1pUA0UiKz0uwGovIaQAAAAACAAAAAAADZgAAwAAAABAAAAAMBxS8fgq+ukz//Un//1/tAAAAAASAAACgAAAAEAAAAOmwEFn1/OqRhJQsOWrp/XUYAAAAQhHmvq5TDdk4utzbrdny//OC/mIug+V7FAAAABzFEl1nxeeYyxT/3+AqwRu48k6h</Password>
    <DbVersion>Azure</DbVersion>
  </Connection>
</Query>

var list = 
	from i in DSP2INFO
	join 
		d in DEVICE on i.MCUID.ToString() equals d.MCUID
	where 
		(d.DeviceTypeID == 6 || d.DeviceTypeID == 7) &&
		d.HWRevision == null
		//d.FirmwareVersion == null
		//(i.AppMajor + "." + i.AppMinor + "." + i.AppBuild + "." + i.AppRevision) != d.FirmwareVersion
	select new {Device = d, Info = i};
	
foreach (var l in list)
{
	var i = l.Info;
	l.Device.FirmwareVersion = i.AppMajor + "." + i.AppMinor + "." + i.AppBuild + "." + i.AppRevision;
	l.Device.BootLoaderVersion = i.BootloaderMajor + "." + i.BootloaderMinor;
	l.Device.HWRevision = i.BoardRev.ToString();
}
SubmitChanges();
//list.Dump();