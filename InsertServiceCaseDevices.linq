<Query Kind="Statements">
  <Connection>
    <ID>e985f619-c073-42e0-9559-9ebfa6233761</ID>
    <Persist>true</Persist>
    <Server>e7a37914t0.database.windows.net</Server>
    <SqlSecurity>true</SqlSecurity>
    <Database>jygi9ig3yy-PIEPS-20150526T220542Z</Database>
    <UserName>sqladministrator</UserName>
    <Password>AQAAANCMnd8BFdERjHoAwE/Cl+sBAAAAaXhJFYUDSUSUP//+rOmn4wAAAAACAAAAAAADZgAAwAAAABAAAAB8zThmv5b6CQpaT7HmH+A9AAAAAASAAACgAAAAEAAAAGDFU1ztWYkXxah1iKJ2DtEQAAAA/rk+QjiQcb3+KlefLbvmBRQAAAA6roNvB817SSYwM2csBDUsFptjcw==</Password>
    <DbVersion>Azure</DbVersion>
    <NoPluralization>true</NoPluralization>
  </Connection>
</Query>

var list = 
	from s in SERVICECASE
    select s;
//list.Dump();
foreach (var s in list)
{
	var d = new SERVICECASE_DEVICE{
		ServiceCaseID = s.ID,
		DeviceTypeID = s.DeviceTypeID,
		DeviceID = s.DeviceID,
		DeviceMalfunction = s.DeviceMalfunction,
		DeviceWithinWarranty = s.DeviceWithinWarranty,
		ProfOfPurchase = s.ProfOfPurchase,
		FaultDescription = s.FaultDescription,
		AgreedCosts = s.Price,
		CustomerRemark = s.CustomerRemark,
		PiepsRemark = s.PiepsRemark
	};
	SERVICECASE_DEVICE.InsertOnSubmit(d);
	SubmitChanges();
}
var devlist = 
	from d in SERVICECASE_DEVICE
	select d;
devlist.Dump();