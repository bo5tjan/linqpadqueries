<Query Kind="Statements">
  <Connection>
    <ID>93be6b62-7a6f-4f23-a554-1841168d3858</ID>
    <Persist>true</Persist>
    <Server>fkmhr5y20c.database.windows.net</Server>
    <SqlSecurity>true</SqlSecurity>
    <Database>PiepsProduction</Database>
    <NoPluralization>true</NoPluralization>
    <UserName>sqladministrator</UserName>
    <Password>AQAAANCMnd8BFdERjHoAwE/Cl+sBAAAAB0sQ1pUA0UiKz0uwGovIaQAAAAACAAAAAAADZgAAwAAAABAAAAAMBxS8fgq+ukz//Un//1/tAAAAAASAAACgAAAAEAAAAOmwEFn1/OqRhJQsOWrp/XUYAAAAQhHmvq5TDdk4utzbrdny//OC/mIug+V7FAAAABzFEl1nxeeYyxT/3+AqwRu48k6h</Password>
    <DbVersion>Azure</DbVersion>
  </Connection>
</Query>

var devices = 
	(from t in TESTRUNRESULT
	where 
		t.TestRunID == 2 &&// Device test
		t.Status == 1 && //OK
		t.FacilityID != 4 &&//not Development
		t.Device.DeviceTypeID == 11 //JF Controller &&
		&& t.DateTime > new DateTime(2015,3,1)
	select t.Device).Distinct();


var q = 
    from d in devices
	where d.DeviceTypeID == 11 && d.PCBSerialNumber != "" && d.HWRevision == "152" /*&& d.CreationDate > new DateTime(2015,4,15)*/
    group d by d.FirmwareVersion into grouped  
    select new { Version = grouped.Key, No = grouped.Count()};
	
q.OrderBy(x=> x.Version).Dump();