<Query Kind="Statements">
  <Connection>
    <ID>82d88dfb-68cd-46e5-a9ce-65c7b988103a</ID>
    <Persist>true</Persist>
    <Server>jygi9ig3yy.database.windows.net</Server>
    <SqlSecurity>true</SqlSecurity>
    <Database>PIEPS</Database>
    <NoPluralization>true</NoPluralization>
    <UserName>sqladministrator</UserName>
    <Password>AQAAANCMnd8BFdERjHoAwE/Cl+sBAAAARLwRB/koWku71ZN/tcuMegAAAAACAAAAAAADZgAAwAAAABAAAADkh94NK8dm92peVbs93zGrAAAAAASAAACgAAAAEAAAAMFZJ2UTCv2Ji9kfS6IOCZoQAAAA1CzbLyBrXkFqHXzBAgO91hQAAAAGakKAf6HDOEUhDWXJP5QM9ZzCqg==</Password>
    <DbVersion>Azure</DbVersion>
  </Connection>
</Query>

var tmp= 
from d in DEVICE
where 
	d.PurchaseDate > new DateTime(2012, 9, 1) &&
	d.PurchaseDate < new DateTime(2013, 9, 1)
group d by d.PurchaseCountryID into dg
select new {
	CountryID = dg.Key,
	Devices = dg.Count()	
};

var results = 
from d in tmp
join c in COUNTRY_LANGUAGES on d.CountryID equals c.CountryID
where c.LanguageID == 1
select new {
	Country = c.Name,
	//ISOCode = c.ISOCode,
	Devices = d.Devices
};
results.OrderByDescending(x=>x.Devices).Dump();
