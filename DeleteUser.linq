<Query Kind="Statements">
  <Connection>
    <ID>82d88dfb-68cd-46e5-a9ce-65c7b988103a</ID>
    <Persist>true</Persist>
    <Server>jygi9ig3yy.database.windows.net</Server>
    <SqlSecurity>true</SqlSecurity>
    <Database>PIEPS</Database>
    <NoPluralization>true</NoPluralization>
    <UserName>sqladministrator</UserName>
    <Password>AQAAANCMnd8BFdERjHoAwE/Cl+sBAAAARLwRB/koWku71ZN/tcuMegAAAAACAAAAAAADZgAAwAAAABAAAADkh94NK8dm92peVbs93zGrAAAAAASAAACgAAAAEAAAAMFZJ2UTCv2Ji9kfS6IOCZoQAAAA1CzbLyBrXkFqHXzBAgO91hQAAAAGakKAf6HDOEUhDWXJP5QM9ZzCqg==</Password>
    <DbVersion>Azure</DbVersion>
  </Connection>
</Query>

var list =
(from u in USER
where u.EMail == "bostjan.sodja@gmail.com"
select u).ToList();

list.Dump();
foreach(var usr in list)
{
	var claims = SERVICECASE.Where(d=> d.ShopID == usr.UserID || d.CustomerID == usr.UserID);
	foreach (var claim in claims){
	
		var progs = SERVICECASE_PROGRESS.Where(p=> p.ServiceCaseID == claim.ID);
		foreach (var prog in progs){
		   SERVICECASE_PROGRESS.DeleteOnSubmit(prog);
		}	
		SERVICECASE.DeleteOnSubmit(claim);
	}
	var fleets = FLEET.Where(f=> f.UserID == usr.UserID);
	foreach (var fleet in fleets){
	  var fleetDevices = FLEET_DEVICE.Where(fd=> fd.FleetID == fleet.FleetID);
	  foreach (var fleetDevice in fleetDevices){
	  		FLEET_DEVICE.DeleteOnSubmit(fleetDevice);
	  }
	  FLEET.DeleteOnSubmit(fleet);
	}	
	var devices = DEVICE.Where(d=> d.UserID == usr.UserID);
	foreach (var device in devices){		
		/*var claims2 = SERVICECASE.Where(d => d.DeviceID == device.DeviceID);
		foreach (var claim2 in claims2){
			SERVICECASE.DeleteOnSubmit(claim2);
		}*/				
		DEVICE.DeleteOnSubmit(device);						
	}
	
	
	USER.DeleteOnSubmit(usr);
}
SubmitChanges();