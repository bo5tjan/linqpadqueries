<Query Kind="Statements">
  <Connection>
    <ID>73d9d0cb-aa68-4515-8753-8f10f540f0cf</ID>
    <Persist>true</Persist>
    <Server>u0pvq7nrpz.database.windows.net</Server>
    <SqlSecurity>true</SqlSecurity>
    <Database>PiepsTest</Database>
    <UserName>sqladministrator</UserName>
    <Password>AQAAANCMnd8BFdERjHoAwE/Cl+sBAAAAB0sQ1pUA0UiKz0uwGovIaQAAAAACAAAAAAADZgAAwAAAABAAAADI3SC9w2Pq0nzEAtBtv67uAAAAAASAAACgAAAAEAAAAMUD0Y29QwtB9g9iBqzci/IQAAAA1/b4EFTFkyuvA9FTi9wOwRQAAAA4VfcQ6Agv7kJ/ORUYQ3mT4YaSVQ==</Password>
    <DbVersion>Azure</DbVersion>
  </Connection>
</Query>

var startDate = new DateTime(2013,11,9);
var endDate = new DateTime(2013,11,10);
var facilityId = 1;
var deviceType = 0;
var records =
     from
	 	 ts in TESTSTEPRESULTs     	 
     where
         ts.TestResult.TestRunResult.DateTime > startDate && ts.TestResult.TestRunResult.DateTime < endDate 
         && (facilityId == 0 || facilityId == ts.TestResult.TestRunResult.FacilityID) 
         && (deviceType == 0 || ts.TestResult.TestRunResult.DEVICE.DeviceTypeID == deviceType)     
		 group ts by ts.TestStepID
         into tsg
         select new
         {
             Id = tsg.Key,
             Count = tsg.Count(),
             Ok = tsg.Count(x => x.Status == 1),
			 Failed = tsg.Count(x => x.Status == 2),
             Error = tsg.Count(x => x.Status == 3),
             Avg = tsg.Average(x => (float)x.Result)
         };
records.Dump();