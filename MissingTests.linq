<Query Kind="Expression">
  <Connection>
    <ID>93be6b62-7a6f-4f23-a554-1841168d3858</ID>
    <Persist>true</Persist>
    <Server>fkmhr5y20c.database.windows.net</Server>
    <SqlSecurity>true</SqlSecurity>
    <Database>PiepsProduction</Database>
    <NoPluralization>true</NoPluralization>
    <UserName>sqladministrator</UserName>
    <Password>AQAAANCMnd8BFdERjHoAwE/Cl+sBAAAAB0sQ1pUA0UiKz0uwGovIaQAAAAACAAAAAAADZgAAwAAAABAAAAAMBxS8fgq+ukz//Un//1/tAAAAAASAAACgAAAAEAAAAOmwEFn1/OqRhJQsOWrp/XUYAAAAQhHmvq5TDdk4utzbrdny//OC/mIug+V7FAAAABzFEl1nxeeYyxT/3+AqwRu48k6h</Password>
    <DbVersion>Azure</DbVersion>
  </Connection>
</Query>

//if all produced devices(label printed) from "date" have board + device test in DB.
from p in PRODUCTION
where 
	p.DateTime > new DateTime (2013, 11,1) &&
	p.DEVICE.TESTRUNRESULT.Count()<2 &&
	p.FacilityID != 4	
orderby p.DateTime
select new {
	ProductionFacility = p.FACILITY.Name,
	DateTime = p.DateTime,
	MCUID = p.MCUID,
	NoOfBoardTests = p.DEVICE.TESTRUNRESULT.Count(x=>x.TestRunID==1),
	NoOfDeviceTest = p.DEVICE.TESTRUNRESULT.Count(x=>x.TestRunID==2)
}