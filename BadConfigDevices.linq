<Query Kind="Statements">
  <Connection>
    <ID>93be6b62-7a6f-4f23-a554-1841168d3858</ID>
    <Persist>true</Persist>
    <Server>fkmhr5y20c.database.windows.net</Server>
    <SqlSecurity>true</SqlSecurity>
    <Database>PiepsProduction</Database>
    <NoPluralization>true</NoPluralization>
    <UserName>sqladministrator</UserName>
    <Password>AQAAANCMnd8BFdERjHoAwE/Cl+sBAAAAB0sQ1pUA0UiKz0uwGovIaQAAAAACAAAAAAADZgAAwAAAABAAAAAMBxS8fgq+ukz//Un//1/tAAAAAASAAACgAAAAEAAAAOmwEFn1/OqRhJQsOWrp/XUYAAAAQhHmvq5TDdk4utzbrdny//OC/mIug+V7FAAAABzFEl1nxeeYyxT/3+AqwRu48k6h</Password>
    <DbVersion>Azure</DbVersion>
  </Connection>
</Query>

//not ok devices - with just one config
var notOk =
from sc in DSP2CONFIG
where 
sc.AntTuneSrReceiveXT0 == 0 || sc.AntTuneSrReceiveXT1 == 0 ||
sc.AntTuneSrReceiveYT0 == 0 || sc.AntTuneSrReceiveYT1 == 0 ||
sc.AntTuneSrReceiveZT0 == 0 || sc.AntTuneSrReceiveZT1 == 0 ||
sc.AntTuneVoltReceiveXT0 == 0 || sc.AntTuneVoltReceiveXT1 == 0 ||
sc.AntTuneVoltReceiveYT0 == 0 || sc.AntTuneVoltReceiveYT1 == 0 ||
sc.AntTuneVoltReceiveZT0 == 0 || sc.AntTuneVoltReceiveZT1 == 0 ||
(sc.DSPPhT1 == 0 && sc.DSPPhT0 == 0) || 
sc.DSPFdev== 0 ||
(sc.DSPFactorX == 1 && sc.DSPFactorY == 1 && sc.DSPFactorZ == 1 )
select sc.MCUID;

//get number of configs for not ok devices
var noOfConfigs = 
from sc in DSP2CONFIG
where notOk.Contains(sc.MCUID)
group sc by sc.MCUID into grp 
select new {
	MCUID = grp.Key,
	Count = grp.Count()
	
};
// just the one wiht one config
var oneConfig = 
from d in noOfConfigs
where d.Count == 1
select d;
//oneConfig.Dump();

//configs of not ok - just one config
var q=
from d in oneConfig
join sc in DSP2CONFIG on d.MCUID equals sc.MCUID
join p in PRODUCTION on d.MCUID equals p.MCUID
join de in DEVICE on d.MCUID equals de.MCUID
select new {
Facility= p.FACILITY.Name,
ProductionDate = p.DateTime,
DeviceType =de.DEVICETYPE.Name,
d.MCUID,
de.SerialNumber,
PCBSerialNumber = de.ProductionSerialNumber,
sc.AntTuneSrReceiveXT0, sc.AntTuneSrReceiveXT1,
sc.AntTuneSrReceiveYT0, sc.AntTuneSrReceiveYT1,
sc.AntTuneSrReceiveZT0, sc.AntTuneSrReceiveZT1,
sc.AntTuneVoltReceiveXT0, sc.AntTuneVoltReceiveXT1,
sc.AntTuneVoltReceiveYT0, sc.AntTuneVoltReceiveYT1,
sc.AntTuneVoltReceiveZT0, sc.AntTuneVoltReceiveZT1,
sc.DSPPhT1, sc.DSPPhT0,
sc.DSPFdev,
sc.DSPFactorX, sc.DSPFactorY, sc.DSPFactorZ
};
q.Dump();

//var t = q.Select(x=> x.MCUID).Distinct();
//t.Dump();