<Query Kind="Statements">
  <Connection>
    <ID>93be6b62-7a6f-4f23-a554-1841168d3858</ID>
    <Persist>true</Persist>
    <Server>fkmhr5y20c.database.windows.net</Server>
    <SqlSecurity>true</SqlSecurity>
    <Database>PiepsProduction</Database>
    <NoPluralization>true</NoPluralization>
    <UserName>sqladministrator</UserName>
    <Password>AQAAANCMnd8BFdERjHoAwE/Cl+sBAAAAB0sQ1pUA0UiKz0uwGovIaQAAAAACAAAAAAADZgAAwAAAABAAAAAMBxS8fgq+ukz//Un//1/tAAAAAASAAACgAAAAEAAAAOmwEFn1/OqRhJQsOWrp/XUYAAAAQhHmvq5TDdk4utzbrdny//OC/mIug+V7FAAAABzFEl1nxeeYyxT/3+AqwRu48k6h</Password>
    <DbVersion>Azure</DbVersion>
  </Connection>
</Query>

var failed = (from t in TESTRUNRESULT
where 
	t.FacilityID == 5 // Anton Paar
	&& t.Status != 1// Not OK
	&& t.Device.DeviceTypeID == 11 	//controller
select t.DeviceID).Distinct();

//failed.Dump();

var ok = (from t in TESTRUNRESULT
where 
	t.FacilityID == 5 // Anton Paar
	&& t.Status == 1// OK
	&& t.Device.DeviceTypeID == 11 	//controller
select t.DeviceID).Distinct();

//ok.Dump();

var inter = ok.Intersect(failed);
inter.Dump();

