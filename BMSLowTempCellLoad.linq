<Query Kind="Statements">
  <Connection>
    <ID>93be6b62-7a6f-4f23-a554-1841168d3858</ID>
    <Persist>true</Persist>
    <Server>fkmhr5y20c.database.windows.net</Server>
    <SqlSecurity>true</SqlSecurity>
    <Database>PiepsProduction</Database>
    <NoPluralization>true</NoPluralization>
    <UserName>sqladministrator</UserName>
    <Password>AQAAANCMnd8BFdERjHoAwE/Cl+sBAAAAB0sQ1pUA0UiKz0uwGovIaQAAAAACAAAAAAADZgAAwAAAABAAAAAMBxS8fgq+ukz//Un//1/tAAAAAASAAACgAAAAEAAAAOmwEFn1/OqRhJQsOWrp/XUYAAAAQhHmvq5TDdk4utzbrdny//OC/mIug+V7FAAAABzFEl1nxeeYyxT/3+AqwRu48k6h</Password>
    <DbVersion>Azure</DbVersion>
  </Connection>
</Query>

var start = new DateTime(2014,9, 4);
var stop = new DateTime(2015,3,10);
var temp1 = 
    from 
		tr in TESTRUNRESULT
    where
		tr.TestRunID== 2 &&		
		tr.DateTime > start && tr.DateTime < stop &&
		tr.FacilityID == 6  &&
		tr.TypeID == 2
	select new {
		tr.DeviceID,
		tr.DateTime,
		tr.Status,
		LowTempCellState = tr.TESTRESULT.FirstOrDefault(x=> x.TestID == 2065),
		LowTemp30ALoad = tr.TESTRESULT.FirstOrDefault(x=> x.TestID == 2066)
	};
	
var temp2 = 
	from 
		t  in temp1
	where 
		t.LowTempCellState != null &&
		t.LowTemp30ALoad != null
	select t;
	
//temp2.Dump();	

var temp3 = 
	from 
		r in temp2
	select new {
		r.DeviceID,
		r.DateTime,
		r.Status,
		Temp = r.LowTempCellState.TESTSTEPRESULT.FirstOrDefault(x=> x.TestStepID == 3182),
		Cell1 = r.LowTempCellState.TESTSTEPRESULT.FirstOrDefault(x=> x.TestStepID == 3183),
		Cell2 = r.LowTempCellState.TESTSTEPRESULT.FirstOrDefault(x=> x.TestStepID == 3184),
		Cell3 = r.LowTempCellState.TESTSTEPRESULT.FirstOrDefault(x=> x.TestStepID == 3185),
		Cell4 = r.LowTempCellState.TESTSTEPRESULT.FirstOrDefault(x=> x.TestStepID == 3186),
		Cell5 = r.LowTempCellState.TESTSTEPRESULT.FirstOrDefault(x=> x.TestStepID == 3187),
		Cell6 = r.LowTempCellState.TESTSTEPRESULT.FirstOrDefault(x=> x.TestStepID == 3188),
		Cell7 = r.LowTempCellState.TESTSTEPRESULT.FirstOrDefault(x=> x.TestStepID == 3189),
		Cell8 = r.LowTempCellState.TESTSTEPRESULT.FirstOrDefault(x=> x.TestStepID == 3190),
		Cell2s1 = r.LowTemp30ALoad.TESTSTEPRESULT.FirstOrDefault(x=> x.TestStepID == 3196),
		Cell2s2 = r.LowTemp30ALoad.TESTSTEPRESULT.FirstOrDefault(x=> x.TestStepID == 3197),
		Cell2s3 = r.LowTemp30ALoad.TESTSTEPRESULT.FirstOrDefault(x=> x.TestStepID == 3198),
		Cell2s4 = r.LowTemp30ALoad.TESTSTEPRESULT.FirstOrDefault(x=> x.TestStepID == 3199),
		Cell2s5 = r.LowTemp30ALoad.TESTSTEPRESULT.FirstOrDefault(x=> x.TestStepID == 3200),
		Cell2s6 = r.LowTemp30ALoad.TESTSTEPRESULT.FirstOrDefault(x=> x.TestStepID == 3201),
		Cell2s7 = r.LowTemp30ALoad.TESTSTEPRESULT.FirstOrDefault(x=> x.TestStepID == 3202),
		Cell2s8 = r.LowTemp30ALoad.TESTSTEPRESULT.FirstOrDefault(x=> x.TestStepID == 3203),		
		Cell4s1 = r.LowTemp30ALoad.TESTSTEPRESULT.FirstOrDefault(x=> x.TestStepID == 3206),
		Cell4s2 = r.LowTemp30ALoad.TESTSTEPRESULT.FirstOrDefault(x=> x.TestStepID == 3207),
		Cell4s3 = r.LowTemp30ALoad.TESTSTEPRESULT.FirstOrDefault(x=> x.TestStepID == 3208),
		Cell4s4 = r.LowTemp30ALoad.TESTSTEPRESULT.FirstOrDefault(x=> x.TestStepID == 3209),
		Cell4s5 = r.LowTemp30ALoad.TESTSTEPRESULT.FirstOrDefault(x=> x.TestStepID == 3210),
		Cell4s6 = r.LowTemp30ALoad.TESTSTEPRESULT.FirstOrDefault(x=> x.TestStepID == 3211),
		Cell4s7 = r.LowTemp30ALoad.TESTSTEPRESULT.FirstOrDefault(x=> x.TestStepID == 3212),
		Cell4s8 = r.LowTemp30ALoad.TESTSTEPRESULT.FirstOrDefault(x=> x.TestStepID == 3213),
		Cell6s1 = r.LowTemp30ALoad.TESTSTEPRESULT.FirstOrDefault(x=> x.TestStepID == 3216),
		Cell6s2 = r.LowTemp30ALoad.TESTSTEPRESULT.FirstOrDefault(x=> x.TestStepID == 3217),
		Cell6s3 = r.LowTemp30ALoad.TESTSTEPRESULT.FirstOrDefault(x=> x.TestStepID == 3218),
		Cell6s4 = r.LowTemp30ALoad.TESTSTEPRESULT.FirstOrDefault(x=> x.TestStepID == 3219),
		Cell6s5 = r.LowTemp30ALoad.TESTSTEPRESULT.FirstOrDefault(x=> x.TestStepID == 3220),
		Cell6s6 = r.LowTemp30ALoad.TESTSTEPRESULT.FirstOrDefault(x=> x.TestStepID == 3221),
		Cell6s7 = r.LowTemp30ALoad.TESTSTEPRESULT.FirstOrDefault(x=> x.TestStepID == 3222),
		Cell6s8 = r.LowTemp30ALoad.TESTSTEPRESULT.FirstOrDefault(x=> x.TestStepID == 3223),		
		Cell8s1 = r.LowTemp30ALoad.TESTSTEPRESULT.FirstOrDefault(x=> x.TestStepID == 3226),
		Cell8s2 = r.LowTemp30ALoad.TESTSTEPRESULT.FirstOrDefault(x=> x.TestStepID == 3227),
		Cell8s3 = r.LowTemp30ALoad.TESTSTEPRESULT.FirstOrDefault(x=> x.TestStepID == 3228),
		Cell8s4 = r.LowTemp30ALoad.TESTSTEPRESULT.FirstOrDefault(x=> x.TestStepID == 3229),
		Cell8s5 = r.LowTemp30ALoad.TESTSTEPRESULT.FirstOrDefault(x=> x.TestStepID == 3230),
		Cell8s6 = r.LowTemp30ALoad.TESTSTEPRESULT.FirstOrDefault(x=> x.TestStepID == 3231),
		Cell8s7 = r.LowTemp30ALoad.TESTSTEPRESULT.FirstOrDefault(x=> x.TestStepID == 3232),
		Cell8s8 = r.LowTemp30ALoad.TESTSTEPRESULT.FirstOrDefault(x=> x.TestStepID == 3233),
		Cell10s1 = r.LowTemp30ALoad.TESTSTEPRESULT.FirstOrDefault(x=> x.TestStepID == 3236),
		Cell10s2 = r.LowTemp30ALoad.TESTSTEPRESULT.FirstOrDefault(x=> x.TestStepID == 3237),
		Cell10s3 = r.LowTemp30ALoad.TESTSTEPRESULT.FirstOrDefault(x=> x.TestStepID == 3238),
		Cell10s4 = r.LowTemp30ALoad.TESTSTEPRESULT.FirstOrDefault(x=> x.TestStepID == 3239),
		Cell10s5 = r.LowTemp30ALoad.TESTSTEPRESULT.FirstOrDefault(x=> x.TestStepID == 3240),
		Cell10s6 = r.LowTemp30ALoad.TESTSTEPRESULT.FirstOrDefault(x=> x.TestStepID == 3241),
		Cell10s7 = r.LowTemp30ALoad.TESTSTEPRESULT.FirstOrDefault(x=> x.TestStepID == 3242),
		Cell10s8 = r.LowTemp30ALoad.TESTSTEPRESULT.FirstOrDefault(x=> x.TestStepID == 3243)
	};

var results = 
	from
		r in temp3
	select new {
		r.DeviceID,
		r.DateTime,
		r.Status,
		Temp = (r.Temp == null)? 0 : r.Temp.Result,
		Cell1 = (r.Cell1 == null)? 0 : r.Cell1.Result,
		Cell2 = (r.Cell2 == null)? 0 : r.Cell2.Result,
		Cell3 = (r.Cell3 == null)? 0 : r.Cell3.Result,
		Cell4 = (r.Cell4 == null)? 0 : r.Cell4.Result,
		Cell5 = (r.Cell5 == null)? 0 : r.Cell5.Result,
		Cell6 = (r.Cell6 == null)? 0 : r.Cell6.Result,
		Cell7 = (r.Cell7 == null)? 0 : r.Cell7.Result,
		Cell8 = (r.Cell8 == null)? 0 : r.Cell8.Result,
		Cell2s1 = (r.Cell2s1 == null)? 0 : r.Cell2s1.Result,
		Cell2s2 = (r.Cell2s2 == null)? 0 : r.Cell2s2.Result,
		Cell2s3 = (r.Cell2s3 == null)? 0 : r.Cell2s3.Result,
		Cell2s4 = (r.Cell2s4 == null)? 0 : r.Cell2s4.Result,
		Cell2s5 = (r.Cell2s5 == null)? 0 : r.Cell2s5.Result,
		Cell2s6 = (r.Cell2s6 == null)? 0 : r.Cell2s6.Result,
		Cell2s7 = (r.Cell2s7 == null)? 0 : r.Cell2s7.Result,
		Cell2s8 = (r.Cell2s8 == null)? 0 : r.Cell2s8.Result,
		Cell4s1 = (r.Cell4s1 == null)? 0 : r.Cell4s1.Result,
		Cell4s2 = (r.Cell4s2 == null)? 0 : r.Cell4s2.Result,
		Cell4s3 = (r.Cell4s3 == null)? 0 : r.Cell4s3.Result,
		Cell4s4 = (r.Cell4s4 == null)? 0 : r.Cell4s4.Result,
		Cell4s5 = (r.Cell4s5 == null)? 0 : r.Cell4s5.Result,
		Cell4s6 = (r.Cell4s6 == null)? 0 : r.Cell4s6.Result,
		Cell4s7 = (r.Cell4s7 == null)? 0 : r.Cell4s7.Result,
		Cell4s8 = (r.Cell4s8 == null)? 0 : r.Cell4s8.Result,
		Cell6s1 = (r.Cell6s1 == null)? 0 : r.Cell6s1.Result,
		Cell6s2 = (r.Cell6s2 == null)? 0 : r.Cell6s2.Result,
		Cell6s3 = (r.Cell6s3 == null)? 0 : r.Cell6s3.Result,
		Cell6s4 = (r.Cell6s4 == null)? 0 : r.Cell6s4.Result,
		Cell6s5 = (r.Cell6s5 == null)? 0 : r.Cell6s5.Result,
		Cell6s6 = (r.Cell6s6 == null)? 0 : r.Cell6s6.Result,
		Cell6s7 = (r.Cell6s7 == null)? 0 : r.Cell6s7.Result,
		Cell6s8 = (r.Cell6s8 == null)? 0 : r.Cell6s8.Result,
		Cell8s1 = (r.Cell8s1 == null)? 0 : r.Cell8s1.Result,
		Cell8s2 = (r.Cell8s2 == null)? 0 : r.Cell8s2.Result,
		Cell8s3 = (r.Cell8s3 == null)? 0 : r.Cell8s3.Result,
		Cell8s4 = (r.Cell8s4 == null)? 0 : r.Cell8s4.Result,
		Cell8s5 = (r.Cell8s5 == null)? 0 : r.Cell8s5.Result,
		Cell8s6 = (r.Cell8s6 == null)? 0 : r.Cell8s6.Result,
		Cell8s7 = (r.Cell8s7 == null)? 0 : r.Cell8s7.Result,
		Cell8s8 = (r.Cell8s8 == null)? 0 : r.Cell8s8.Result,
		Cell10s1 = (r.Cell10s1 == null)? 0 : r.Cell10s1.Result,
		Cell10s2 = (r.Cell10s2 == null)? 0 : r.Cell10s2.Result,
		Cell10s3 = (r.Cell10s3 == null)? 0 : r.Cell10s3.Result,
		Cell10s4 = (r.Cell10s4 == null)? 0 : r.Cell10s4.Result,
		Cell10s5 = (r.Cell10s5 == null)? 0 : r.Cell10s5.Result,
		Cell10s6 = (r.Cell10s6 == null)? 0 : r.Cell10s6.Result,
		Cell10s7 = (r.Cell10s7 == null)? 0 : r.Cell10s7.Result,
		Cell10s8 = (r.Cell10s8 == null)? 0 : r.Cell10s8.Result
	};
		
	
results.Dump();
	
    