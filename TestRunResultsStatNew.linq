<Query Kind="Statements">
  <Connection>
    <ID>93be6b62-7a6f-4f23-a554-1841168d3858</ID>
    <Persist>true</Persist>
    <Server>fkmhr5y20c.database.windows.net</Server>
    <SqlSecurity>true</SqlSecurity>
    <Database>PiepsProduction</Database>
    <NoPluralization>true</NoPluralization>
    <UserName>sqladministrator</UserName>
    <Password>AQAAANCMnd8BFdERjHoAwE/Cl+sBAAAAB0sQ1pUA0UiKz0uwGovIaQAAAAACAAAAAAADZgAAwAAAABAAAAAMBxS8fgq+ukz//Un//1/tAAAAAASAAACgAAAAEAAAAOmwEFn1/OqRhJQsOWrp/XUYAAAAQhHmvq5TDdk4utzbrdny//OC/mIug+V7FAAAABzFEl1nxeeYyxT/3+AqwRu48k6h</Password>
    <DbVersion>Azure</DbVersion>
  </Connection>
</Query>

var startDate = new DateTime(2013,5, 1);
var endDate = new DateTime(2014,6,1);
var facilityId = 2;
var userId = -1;
var deviceType = 0;
var testType = 0;
var perMonth = true;

var q = TESTRUNRESULT.Where(p =>
                                p.DateTime > startDate && p.DateTime < endDate &&
                               (facilityId == 0 || p.FacilityID == facilityId) &&
                               (userId == -1 || (userId == 0 && p.UserID == null) || p.UserID == userId) &&
                               (deviceType == 0 || p.Device.DeviceTypeID == deviceType || p.Device.DEVICETYPE.BaseDeviceTypeID == deviceType) &&
                               (testType == 0 || p.TestRunID == testType)
                      )
                        .GroupBy(
                            p =>
                            new { P1 = p.DateTime.Year, P2 = p.DateTime.Month, P3 = perMonth? 0 : p.DateTime.Day })
                        .Select(t => new 
                        {
                            Params = t.Key,
							Number = t.Count(),
                            DevicesOKTests = t.Where(x => x.Status == 1).Select(x => x.DeviceID).Distinct(),
							DevicesFailedTests = t.Where(x => x.Status != 1).Select(x => x.DeviceID).Distinct(),                           
                            TestRunsOK = t.Count(x => x.Status == 1),
                            TestRunsFailed = t.Count(x => x.Status != 1)
                        }).ToList();
																		

var r = from temp in q
		select new {
			Date = temp.Params.P1 + "-" + temp.Params.P2 + "-" + temp.Params.P3,
			Number = temp.Number,
			DevicesOK = temp.DevicesOKTests.Count(),
			DevicesOKOnly = temp.DevicesOKTests.Except(temp.DevicesFailedTests).Count(),
			DevicesFailed = temp.DevicesFailedTests.Except(temp.DevicesOKTests).Count(),
			TestRunsOK = temp.TestRunsOK,
			TestRunsFailed = temp.TestRunsFailed			
		};
r.Dump();
