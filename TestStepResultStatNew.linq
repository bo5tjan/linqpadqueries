<Query Kind="Statements">
  <Connection>
    <ID>43933573-45c6-4ece-a363-a24e21940fdc</ID>
    <Persist>true</Persist>
    <Driver>EntityFrameworkDbContext</Driver>
    <CustomAssemblyPath>D:\work\PiepsProductionPortal\PiepsProductionPortal\bin\PiepsProductionPortal.dll</CustomAssemblyPath>
    <CustomTypeName>PiepsProductionPortal.DB.Prod.PiepsProductionEntities</CustomTypeName>
    <AppConfigPath>D:\work\PiepsProductionPortal\PiepsProductionPortal\bin\PiepsProductionPortal.dll.config</AppConfigPath>
    <DisplayName>PiepsProductionEntities</DisplayName>
  </Connection>
</Query>

var startDate = new DateTime(2014,5, 1);
var endDate = new DateTime(2014,6,6);
var facilityId = 2;
var userId = -1;
var deviceType = 0;
var testType = 0;
var testId = 0;
var testStepStatus = -1;

var temp = 
                   ( from
                        ts in TESTSTEPRESULT
                        /*join t in TESTRESULT on ts.TestResultID equals t.ID
                        join tr in TESTRUNRESULT on t.TestRunResultID equals tr.ID*/
                    
            
                    where                        
                        ts.TESTRESULT.TESTRUNRESULT.DateTime > startDate && ts.TESTRESULT.TESTRUNRESULT.DateTime < endDate                                                 
					select ts);
if (facilityId>0){
	temp = temp.Where(x=> facilityId == x.TESTRESULT.TESTRUNRESULT.FacilityID); 
}
if (userId > -1){
	temp = temp.Where(x=> (userId == 0 && x.TESTRESULT.TESTRUNRESULT.UserID == null) || (x.TESTRESULT.TESTRUNRESULT.UserID == userId));
}
if (testType > 0){
	temp = temp.Where(x=>x.TESTRESULT.TESTRUNRESULT.TestRunID == testType);
}
if (testId > 0){
	temp = temp.Where(x=> x.TESTRESULT.TestID == testId);
}

if (testStepStatus > -1){
	temp = temp.Where(x=> x.Status == testStepStatus);
}

if (deviceType > 0){
	temp = temp.Where(x=> x.TESTRESULT.TESTRUNRESULT.DEVICE.DeviceTypeID == deviceType || x.TESTRESULT.TESTRUNRESULT.DEVICE.DEVICETYPE.BaseDeviceTypeID == deviceType);
}


 	/*temp = (from ts in temp
			where       (userId == -1 || (userId == 0 && ts.TESTRESULT.TESTRUNRESULT.UserID == null) || ts.TESTRESULT.TESTRUNRESULT.UserID == userId) &&
                        (testType == 0 || ts.TESTRESULT.TESTRUNRESULT.TestRunID == testType) &&
                        (testId == 0 || ts.TESTRESULT.TestID == testId) &&
                        (testStepStatus == -1 || ts.Status == testStepStatus) &&
                        (deviceType == 0 || ts.TESTRESULT.TESTRUNRESULT.DEVICE.DeviceTypeID == deviceType || ts.TESTRESULT.TESTRUNRESULT.DEVICE.DEVICETYPE.BaseDeviceTypeID == deviceType) 
			select ts).ToList();*/
			

var temp1 = from ts in temp
                    group ts by ts.TestStepID
                        into tsg
                        select new
                        {
                            Id = tsg.Key,
                            Count = tsg.Count(),
                            Ok = tsg.Count(x => x.Status == 1),
                            Failed = tsg.Count(x => x.Status == 2),
                            Error = tsg.Count(x => x.Status == 3),
                            Avg = tsg.Average(x => (float)x.Result)
                        };
					
						
////temp.Dump();

 var temp2 = from t in temp1
                          join ts in TESTSTEP on t.Id equals ts.TestStepID
                          select new 
                              {
                                  Id = t.Id,
                                  Name = ts.TEST.Name + ": " + ts.Name,
                                  Count = t.Count,
                                  Ok = t.Ok,
                                  Failed = t.Failed,
                                  Error = t.Error,
                                  Avg = t.Avg
                              };
var results = temp2.OrderBy(x => x.Name).ToList();           
results.Dump();