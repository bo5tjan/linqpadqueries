<Query Kind="Expression">
  <Connection>
    <ID>82d88dfb-68cd-46e5-a9ce-65c7b988103a</ID>
    <Persist>true</Persist>
    <Server>jygi9ig3yy.database.windows.net</Server>
    <SqlSecurity>true</SqlSecurity>
    <Database>PIEPS</Database>
    <NoPluralization>true</NoPluralization>
    <UserName>sqladministrator</UserName>
    <Password>AQAAANCMnd8BFdERjHoAwE/Cl+sBAAAARLwRB/koWku71ZN/tcuMegAAAAACAAAAAAADZgAAwAAAABAAAADkh94NK8dm92peVbs93zGrAAAAAASAAACgAAAAEAAAAMFZJ2UTCv2Ji9kfS6IOCZoQAAAA1CzbLyBrXkFqHXzBAgO91hQAAAAGakKAf6HDOEUhDWXJP5QM9ZzCqg==</Password>
    <DbVersion>Azure</DbVersion>
  </Connection>
</Query>

from u in USER
where 
 (u.CountryID == 3 || u.CountryID == 16 )
 && u.UserStatusID == 2
select new {
	u.UserID,
	u.Firstname,
	u.Lastname,
	u.OrganisationName,
	u.EMail,
	u.Street,
	u.ZIP,
	u.City,
	u.Phone,
	Country = (u.CountryID == 3)? "Germany" : "Austria"
	
}