<Query Kind="Statements">
  <Connection>
    <ID>93be6b62-7a6f-4f23-a554-1841168d3858</ID>
    <Persist>true</Persist>
    <Server>fkmhr5y20c.database.windows.net</Server>
    <SqlSecurity>true</SqlSecurity>
    <Database>PiepsProduction</Database>
    <NoPluralization>true</NoPluralization>
    <UserName>sqladministrator</UserName>
    <Password>AQAAANCMnd8BFdERjHoAwE/Cl+sBAAAAB0sQ1pUA0UiKz0uwGovIaQAAAAACAAAAAAADZgAAwAAAABAAAAAMBxS8fgq+ukz//Un//1/tAAAAAASAAACgAAAAEAAAAOmwEFn1/OqRhJQsOWrp/XUYAAAAQhHmvq5TDdk4utzbrdny//OC/mIug+V7FAAAABzFEl1nxeeYyxT/3+AqwRu48k6h</Password>
    <DbVersion>Azure</DbVersion>
  </Connection>
</Query>

var startDate = new DateTime(2014,5, 1);
var endDate = new DateTime(2014,6,6);
var facilityId = 2;
var userId = -1;
var deviceType = 0;
var testType = 0;
var testId = 0;
var testStepStatus = -1;

var temp =
                    from
                        ts in TESTSTEPRESULT
                       // join t in _db.TESTRESULT on ts.TestResultID equals t.ID
                       // join tr in _db.TESTRUNRESULT on t.TestRunResultID equals tr.ID
                    
            
                    where                        
                        ts.TestResult.TestRunResult.DateTime > startDate && ts.TestResult.TestRunResult.DateTime < endDate &&                        
                        (facilityId == 0 || facilityId == ts.TestResult.TestRunResult.FacilityID) &&
                        (userId == -1 || (userId == 0 && ts.TestResult.TestRunResult.UserID == null) || ts.TestResult.TestRunResult.UserID == userId) &&
                        (testType == 0 || ts.TestResult.TestRunResult.TestRunID == testType) &&
                        (testId == 0 || ts.TestResult.TestID == testId) &&
                        (testStepStatus == -1 || ts.Status == testStepStatus) &&
                        (deviceType == 0 || ts.TestResult.TestRunResult.Device.DeviceTypeID == deviceType || ts.TestResult.TestRunResult.Device.DEVICETYPE.BaseDeviceTypeID == deviceType) 

                    group ts by ts.TestStepID
                        into tsg
                        select new
                        {
                            Id = tsg.Key,
                            Count = tsg.Count(),
                            Ok = tsg.Count(x => x.Status == 1),
                            Failed = tsg.Count(x => x.Status == 2),
                            Error = tsg.Count(x => x.Status == 3),
                            Avg = tsg.Average(x => (float)x.Result)
                        };  
						
//temp.Dump();

 var temp2 = from t in temp
                          join ts in TESTSTEP on t.Id equals ts.TestStepID
                          select new 
                              {
                                  Id = t.Id,
                                  Name = ts.TEST.Name + ": " + ts.Name,
                                  Count = t.Count,
                                  Ok = t.Ok,
                                  Failed = t.Failed,
                                  Error = t.Error,
                                  Avg = t.Avg
                              };
var results = temp2.OrderBy(x => x.Name).ToList();
           
results.Dump();
