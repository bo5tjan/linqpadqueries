<Query Kind="Expression">
  <Connection>
    <ID>82d88dfb-68cd-46e5-a9ce-65c7b988103a</ID>
    <Persist>true</Persist>
    <Server>jygi9ig3yy.database.windows.net</Server>
    <SqlSecurity>true</SqlSecurity>
    <Database>PIEPS</Database>
    <NoPluralization>true</NoPluralization>
    <UserName>sqladministrator</UserName>
    <Password>AQAAANCMnd8BFdERjHoAwE/Cl+sBAAAARLwRB/koWku71ZN/tcuMegAAAAACAAAAAAADZgAAwAAAABAAAADkh94NK8dm92peVbs93zGrAAAAAASAAACgAAAAEAAAAMFZJ2UTCv2Ji9kfS6IOCZoQAAAA1CzbLyBrXkFqHXzBAgO91hQAAAAGakKAf6HDOEUhDWXJP5QM9ZzCqg==</Password>
    <DbVersion>Azure</DbVersion>
  </Connection>
</Query>

(from d in DEVICE
where 
	(d.FirmwareRelease.StartsWith("1") || d.FirmwareRelease.StartsWith("2") )
	&& (d.DeviceTypeID == 6 || d.DeviceTypeID == 7)
	//&& (d.USER.CountryID != 3 && d.USER.CountryID != 16)
select new {
  	d.USER.UserID,
	d.USER.EMail,
	d.USER.Firstname,
	d.USER.Lastname,
	d.USER.City,
	Country = d.USER.COUNTRY.Name,
	d.USER.Phone
  }
).Distinct()