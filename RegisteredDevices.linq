<Query Kind="Expression">
  <Connection>
    <ID>82d88dfb-68cd-46e5-a9ce-65c7b988103a</ID>
    <Persist>true</Persist>
    <Server>jygi9ig3yy.database.windows.net</Server>
    <SqlSecurity>true</SqlSecurity>
    <Database>PIEPS</Database>
    <NoPluralization>true</NoPluralization>
    <UserName>sqladministrator</UserName>
    <Password>AQAAANCMnd8BFdERjHoAwE/Cl+sBAAAARLwRB/koWku71ZN/tcuMegAAAAACAAAAAAADZgAAwAAAABAAAADkh94NK8dm92peVbs93zGrAAAAAASAAACgAAAAEAAAAMFZJ2UTCv2Ji9kfS6IOCZoQAAAA1CzbLyBrXkFqHXzBAgO91hQAAAAGakKAf6HDOEUhDWXJP5QM9ZzCqg==</Password>
    <DbVersion>Azure</DbVersion>
  </Connection>
</Query>

from d in DEVICE
join u in USER on d.UserID equals u.UserID
where (d.Deleted ==null || d.Deleted == false)
select new {
	Device = d.DEVICETYPE.Name,
	Serial = d.Serialnumber,
	PurchaseDate = d.PurchaseDate,
	u.Lastname,
	u.Firstname,
	u.OrganisationName,
	u.EMail,
	u.Phone,
	u.Street,
	u.ZIP,
	u.City,
	Country = u.COUNTRY.Name,
	Newsletter = u.UserOptions.Contains("Newsletter=1")
}