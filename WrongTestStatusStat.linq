<Query Kind="Statements">
  <Connection>
    <ID>93be6b62-7a6f-4f23-a554-1841168d3858</ID>
    <Persist>true</Persist>
    <Server>fkmhr5y20c.database.windows.net</Server>
    <SqlSecurity>true</SqlSecurity>
    <Database>PiepsProduction</Database>
    <NoPluralization>true</NoPluralization>
    <UserName>sqladministrator</UserName>
    <Password>AQAAANCMnd8BFdERjHoAwE/Cl+sBAAAAB0sQ1pUA0UiKz0uwGovIaQAAAAACAAAAAAADZgAAwAAAABAAAAAMBxS8fgq+ukz//Un//1/tAAAAAASAAACgAAAAEAAAAOmwEFn1/OqRhJQsOWrp/XUYAAAAQhHmvq5TDdk4utzbrdny//OC/mIug+V7FAAAABzFEl1nxeeYyxT/3+AqwRu48k6h</Password>
    <DbVersion>Azure</DbVersion>
  </Connection>
</Query>

var temp = 
from tc in TESTRESULT 
join ts in TESTSTEPRESULT on tc.ID equals ts.TestResultID
where tc.Status == 1 && ts.Status != 1
group tc by tc.TestID into tcg
select new {
	tcg.Key,
	No = tcg.Count()
};
var results= 
from tr in temp 
join t in TEST on tr.Key equals t.TestID
select new {
	Id = tr.Key,
	Name = t.Name,
	Count = tr.No
};
results.Dump();
