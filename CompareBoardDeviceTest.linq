<Query Kind="Expression">
  <Connection>
    <ID>93be6b62-7a6f-4f23-a554-1841168d3858</ID>
    <Persist>true</Persist>
    <Server>fkmhr5y20c.database.windows.net</Server>
    <SqlSecurity>true</SqlSecurity>
    <Database>PiepsProduction</Database>
    <NoPluralization>true</NoPluralization>
    <UserName>sqladministrator</UserName>
    <Password>AQAAANCMnd8BFdERjHoAwE/Cl+sBAAAAB0sQ1pUA0UiKz0uwGovIaQAAAAACAAAAAAADZgAAwAAAABAAAAAMBxS8fgq+ukz//Un//1/tAAAAAASAAACgAAAAEAAAAOmwEFn1/OqRhJQsOWrp/XUYAAAAQhHmvq5TDdk4utzbrdny//OC/mIug+V7FAAAABzFEl1nxeeYyxT/3+AqwRu48k6h</Password>
    <DbVersion>Azure</DbVersion>
  </Connection>
</Query>

from d in DEVICE
join trB in TESTRUNRESULT on d.MCUID equals trB.MCUID
join tcB in TESTRESULT on trB.ID equals tcB.TestRunResultID
join tsB in TESTSTEPRESULT on tcB.ID equals tsB.TestResult.ID
join trD in TESTRUNRESULT on d.MCUID equals trD.MCUID
join tcD in TESTRESULT on trD.ID equals tcD.TestRunResultID
join tsD in TESTSTEPRESULT on tcD.ID equals tsD.TestResult.ID
where 
	trB.Status == 1 && tcB.Status == 1 && tsB.Status == 1 && trB.TestRunID == 1 && trB.FacilityID != 4 && trB.DateTime > new DateTime(2013,11,1) &&
	tsB.TestStepID == 1068 && 
	trD.Status == 1 && tcD.Status == 1 && tsD.Status == 1 && trD.TestRunID == 2 && trD.FacilityID != 4 && trD.DateTime > new DateTime(2013,11,1) &&
	tsD.TestStepID == tsB.TestStepID 
select new {
	MCUID = d.MCUID,
	DeviceType = d.DeviceTypeID,
	BoardTestFacility = trB.FacilityID,
	BoardTestDate = trB.DateTime,
	BoardTestResult = tsB.Result,
	DeviceTestFacility = trD.FacilityID,
	DeviceTestDate = trD.DateTime,
	DeviceTestResult = tsD.Result,
	ResultDiff = tsB.Result - tsD.Result	
}