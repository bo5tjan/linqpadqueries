<Query Kind="Statements">
  <Connection>
    <ID>82d88dfb-68cd-46e5-a9ce-65c7b988103a</ID>
    <Persist>true</Persist>
    <Server>jygi9ig3yy.database.windows.net</Server>
    <SqlSecurity>true</SqlSecurity>
    <Database>PIEPS</Database>
    <NoPluralization>true</NoPluralization>
    <UserName>sqladministrator</UserName>
    <Password>AQAAANCMnd8BFdERjHoAwE/Cl+sBAAAARLwRB/koWku71ZN/tcuMegAAAAACAAAAAAADZgAAwAAAABAAAADkh94NK8dm92peVbs93zGrAAAAAASAAACgAAAAEAAAAMFZJ2UTCv2Ji9kfS6IOCZoQAAAA1CzbLyBrXkFqHXzBAgO91hQAAAAGakKAf6HDOEUhDWXJP5QM9ZzCqg==</Password>
    <DbVersion>Azure</DbVersion>
  </Connection>
</Query>

// parse list into data
var data =
from row in MyExtensions.ReadFrom(@"d:\DevService1.txt").Cast<string>()
let columns = row.Split('\t')
select new
{
 SourceId = int.Parse(columns[0]),
 CustomerNo = int.Parse(columns[1]),
 Name = columns[2],
 Date = DateTime.Parse(columns[5]),
 WarrantyNo = columns[6],
 Device = columns[7],
 SN = columns[8].Length == 14? columns[8].Substring(0,10): columns[8],
 Type = columns[11]
};

// found devices in DB
var ids = data.Select(x=>x.SN).ToList();
var devices = from d in DEVICE
where ids.Contains(d.Serialnumber) 
select d;

// update devices - Last DeviceService
foreach (var d in devices)
{
	d.LastDeviceService = data.FirstOrDefault(x=> x.SN == d.Serialnumber).Date;	
}
//SubmitChanges();
devices.Dump();