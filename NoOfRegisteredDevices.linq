<Query Kind="Expression">
  <Connection>
    <ID>82d88dfb-68cd-46e5-a9ce-65c7b988103a</ID>
    <Persist>true</Persist>
    <Server>jygi9ig3yy.database.windows.net</Server>
    <SqlSecurity>true</SqlSecurity>
    <Database>PIEPS</Database>
    <NoPluralization>true</NoPluralization>
    <UserName>sqladministrator</UserName>
    <Password>AQAAANCMnd8BFdERjHoAwE/Cl+sBAAAARLwRB/koWku71ZN/tcuMegAAAAACAAAAAAADZgAAwAAAABAAAADkh94NK8dm92peVbs93zGrAAAAAASAAACgAAAAEAAAAMFZJ2UTCv2Ji9kfS6IOCZoQAAAA1CzbLyBrXkFqHXzBAgO91hQAAAAGakKAf6HDOEUhDWXJP5QM9ZzCqg==</Password>
    <DbVersion>Azure</DbVersion>
  </Connection>
</Query>

(from d in DEVICE 
where 
	d.PurchaseDate > new DateTime(2014, 9, 1) &&
	d.PurchaseDate < new DateTime(2015, 9, 1)
group d by new {
	d.PurchaseDate.Year,
	d.PurchaseDate.Month
	
} into dg
select new {
	Month = dg.Key.Year + "-" + ((dg.Key.Month<10)? "0" : "") + dg.Key.Month,
	All = dg.Count(),
	DSP = dg.Count(x => x.DeviceTypeID == 1),
	DSPTour = dg.Count(x => x.DeviceTypeID == 2),
	DSPSport = dg.Count(x => x.DeviceTypeID == 6),
	DSPPro = dg.Count(x => x.DeviceTypeID == 7),
	Jetforce = dg.Count(x => x.DeviceTypeID == 21 || x.DeviceTypeID == 22 )
}).OrderBy(stat=> stat.Month)