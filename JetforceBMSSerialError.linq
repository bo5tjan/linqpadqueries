<Query Kind="Statements">
  <Connection>
    <ID>93be6b62-7a6f-4f23-a554-1841168d3858</ID>
    <Persist>true</Persist>
    <Server>fkmhr5y20c.database.windows.net</Server>
    <SqlSecurity>true</SqlSecurity>
    <Database>PiepsProduction</Database>
    <NoPluralization>true</NoPluralization>
    <UserName>sqladministrator</UserName>
    <Password>AQAAANCMnd8BFdERjHoAwE/Cl+sBAAAAB0sQ1pUA0UiKz0uwGovIaQAAAAACAAAAAAADZgAAwAAAABAAAAAMBxS8fgq+ukz//Un//1/tAAAAAASAAACgAAAAEAAAAOmwEFn1/OqRhJQsOWrp/XUYAAAAQhHmvq5TDdk4utzbrdny//OC/mIug+V7FAAAABzFEl1nxeeYyxT/3+AqwRu48k6h</Password>
    <DbVersion>Azure</DbVersion>
  </Connection>
</Query>

var list = 
(from t in TESTRUNRESULT 
where 
t.Device.DeviceTypeID == 12 //BMS
&& t.Device.PCBSerialNumber.Length == 12
&& t.FacilityID == 6 //SIPA
select new 
{ 
  t.ID,
  DeviceID = t.Device.ID,
  PCBSerialNumber = long.Parse(t.Device.PCBSerialNumber)
}).ToList();

//list.Dump();

var list2 = 
from t in list
select new { 
 t.ID,
 t.DeviceID,
 t.PCBSerialNumber,    
 NewSerialNumber = (t.PCBSerialNumber / 256).ToString()
};
//list2.Dump();


// get new DeviceID
var listdevice = 
from t in list2
join d in DEVICE on  t.NewSerialNumber equals d.PCBSerialNumber
select new {
 t.ID,
 t.DeviceID,
 t.PCBSerialNumber,    
 t.NewSerialNumber,
 NewDeviceID = d.ID	
};
listdevice.Dump();
foreach (var result in listdevice)
{
	var t = TESTRUNRESULT.Where(x=> x.ID == result.ID).FirstOrDefault();
	t.DeviceID = result.NewDeviceID;
	t.Dump();		
}
SubmitChanges();