<Query Kind="Statements">
  <Connection>
    <ID>93be6b62-7a6f-4f23-a554-1841168d3858</ID>
    <Persist>true</Persist>
    <Server>fkmhr5y20c.database.windows.net</Server>
    <SqlSecurity>true</SqlSecurity>
    <Database>PiepsProduction</Database>
    <NoPluralization>true</NoPluralization>
    <UserName>sqladministrator</UserName>
    <Password>AQAAANCMnd8BFdERjHoAwE/Cl+sBAAAAB0sQ1pUA0UiKz0uwGovIaQAAAAACAAAAAAADZgAAwAAAABAAAAAMBxS8fgq+ukz//Un//1/tAAAAAASAAACgAAAAEAAAAOmwEFn1/OqRhJQsOWrp/XUYAAAAQhHmvq5TDdk4utzbrdny//OC/mIug+V7FAAAABzFEl1nxeeYyxT/3+AqwRu48k6h</Password>
    <DbVersion>Azure</DbVersion>
  </Connection>
</Query>

var listTS =
(from ts in TESTSTEPRESULT
where 
	ts.TestResult.TestRunResult.DateTime >= new DateTime(2014,4,14)
	&& ts.TestResult.TestRunResult.FacilityID == 4
	&& ts.TestResult.TestRunResult.UserID == 10
select ts).ToList();
listTS.Dump();
foreach(var ts in listTS)
{
	TESTSTEPRESULT.DeleteOnSubmit(ts);
}
SubmitChanges();


var listTC =
(from tc in TESTRESULT
where 
	tc.TestRunResult.DateTime >= new DateTime(2014,4,14)
	&& tc.TestRunResult.FacilityID == 4
	&& tc.TestRunResult.UserID == 10
select tc).ToList();
listTC.Dump();
foreach(var tc in listTC)
{
	TESTRESULT.DeleteOnSubmit(tc);
}
SubmitChanges();


var listTR =
(from tr in TESTRUNRESULT
where 
	tr.DateTime >= new DateTime(2014,4,14)
	&& tr.FacilityID == 4
	&& tr.UserID == 10
select tr).ToList();
listTR.Dump();
foreach(var tr in listTR)
{
	TESTRUNRESULT.DeleteOnSubmit(tr);
}
SubmitChanges();


