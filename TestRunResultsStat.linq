<Query Kind="Statements">
  <Connection>
    <ID>93be6b62-7a6f-4f23-a554-1841168d3858</ID>
    <Persist>true</Persist>
    <Server>fkmhr5y20c.database.windows.net</Server>
    <SqlSecurity>true</SqlSecurity>
    <Database>PiepsProduction</Database>
    <NoPluralization>true</NoPluralization>
    <UserName>sqladministrator</UserName>
    <Password>AQAAANCMnd8BFdERjHoAwE/Cl+sBAAAAB0sQ1pUA0UiKz0uwGovIaQAAAAACAAAAAAADZgAAwAAAABAAAAAMBxS8fgq+ukz//Un//1/tAAAAAASAAACgAAAAEAAAAOmwEFn1/OqRhJQsOWrp/XUYAAAAQhHmvq5TDdk4utzbrdny//OC/mIug+V7FAAAABzFEl1nxeeYyxT/3+AqwRu48k6h</Password>
    <DbVersion>Azure</DbVersion>
  </Connection>
</Query>

var startDate = new DateTime(2014,6, 1);
var endDate = new DateTime(2014,6,6);
var facilityId = 2;
var userId = -1;
var deviceType = 0;
var testType = 0;


var q = TESTRUNRESULT.Where(p =>
                                p.DateTime > startDate && p.DateTime < endDate &&
                               (facilityId == 0 || p.FacilityID == facilityId) &&
                               (userId == -1 || (userId == 0 && p.UserID == null) || p.UserID == userId) &&
                               (deviceType == 0 || p.Device.DeviceTypeID == deviceType || p.Device.DEVICETYPE.BaseDeviceTypeID == deviceType) &&
                               (testType == 0 || p.TestRunID == testType)
                      )
                        .GroupBy(
                            p =>
                            new { P1 = p.DateTime.Year, P2 = p.DateTime.Month, P3 = p.DateTime.Day })
                        .Select(t => new 
                        {
                            Params = t.Key,
							Number = t.Count(),
                            DevicesOKTests = t.Where(x => x.Status == 1).Select(x => x.DeviceID).Distinct().Count(),
							DevicesFailedTests = t.Where(x => x.Status != 1).Select(x => x.DeviceID).Distinct().Count(),
                            DevicesOKOnly =
                                t.Where(x => x.Status == 1)
                                 .Select(x => x.DeviceID)
                                 .Except(from y in t where y.Status != 1 select y.DeviceID)
                                 .Distinct()
                                 .Count(),
                            DevicesFailed =
                                t.Select(x => x.DeviceID)
                                 .Except(from y in t where y.Status == 1 select y.DeviceID)
                                 .Distinct()
                                 .Count(),
                            TestRunsOK = t.Count(x => x.Status == 1),
                            TestRunsFailed = t.Count(x => x.Status != 1)
                        });
						
q.Dump();

/*var first =  q.FirstOrDefault();
var result = new {
	Params = first.Params,
	Number = first.Number,
	TestRunsOK = first.TestRunsOK,
	TestRunsFailed = first.TestRunsFailed,
	DevicesOKOnly = first.DevicesOKTests.Except(first.DevicesFailedTests).Count(),
	DevicesFailedTests = first.DevicesFailedTests.Except(first.DevicesOKTests).Count()
};
result.Dump();*/
//first.Dump();