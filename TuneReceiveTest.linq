<Query Kind="Expression">
  <Connection>
    <ID>93be6b62-7a6f-4f23-a554-1841168d3858</ID>
    <Persist>true</Persist>
    <Server>fkmhr5y20c.database.windows.net</Server>
    <SqlSecurity>true</SqlSecurity>
    <Database>PiepsProduction</Database>
    <NoPluralization>true</NoPluralization>
    <UserName>sqladministrator</UserName>
    <Password>AQAAANCMnd8BFdERjHoAwE/Cl+sBAAAAB0sQ1pUA0UiKz0uwGovIaQAAAAACAAAAAAADZgAAwAAAABAAAAAMBxS8fgq+ukz//Un//1/tAAAAAASAAACgAAAAEAAAAOmwEFn1/OqRhJQsOWrp/XUYAAAAQhHmvq5TDdk4utzbrdny//OC/mIug+V7FAAAABzFEl1nxeeYyxT/3+AqwRu48k6h</Password>
    <DbVersion>Azure</DbVersion>
  </Connection>
</Query>

from ts in TESTSTEPRESULT
where 
	//(ts.TestStepID == 1042 || ts.TestStepID == 1066) ||
	//(ts.TestStepID == 1039 || ts.TestStepID == 1067) ||
	(ts.TestStepID == 1036 || ts.TestStepID == 1068)
	&& (ts.TestResult.TestRunResult.FacilityID != 4 && ts.TestResult.TestRunResult.TestRunID < 3)
select new { 
	DeviceId = ts.TestResult.TestRunResult.DeviceID, 
	FacilityId = ts.TestResult.TestRunResult.FacilityID,
	Type = ts.TestResult.TestRunResult.TestRun.Name,
	TestCase = ts.TestResult.TEST.Name,
	TestStep = ts.TESTSTEP.Name,	
	Status = ts.StatusRESULTTYPE.Name,
	Result = ts.Result
}
	